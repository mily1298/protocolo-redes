#!/usr/bin/env python3
from mysql.connector import Error
import mysql.connector
import socket 
import struct
import threading
from Modelos.clienteModelo import *
from Modelos.pokemon import *
import sys
import os

ConsultaPokes = 11
CapturaPoke = 20
pokemonCap = 27
CapturaOtra = 21
ConsultaPokemones =26
EnviaPoke = 22
CapturaTermina = 23
Si = 30
No = 31
TerminaSesion = 32
InicioBien = 8
InicioMal = 7

class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        print("Instanciado")
        

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(360)
            print("conectado")
            threading.Thread(target=self.listenToClient,
                             args=(client, address)).start()
        
    def listenToClient(self, client, address):
        try:
            while True:
                buf = Buffer(client)
                code = bytes_a_int(buf.get(1))
                if code == 9:
                    lengthBuf1 = struct.unpack('<B', buf.get(1))[0]
                    correoByte = buf.get(lengthBuf1)
                    lengthBuf2 = struct.unpack('<B', buf.get(1))[0]
                    contraseniaByte = buf.get(lengthBuf2)
                    correo = correoByte.decode()
                    contrasenia = contraseniaByte.decode()
                    clienteDB = ClienteModelo(correo, contrasenia)
                    clienteDB.verificarUsuario()
                    if clienteDB.usuarioExistente == True:
                        client.send(struct.pack('<B', InicioBien))
                        clienteDB.listaPokemones()
                    else:
                        client.send(struct.pack('<B', InicioMal))
                elif code == 10:
                    pokemonAtrapar = clienteDB.randomPokemon()
                    idPokemon = pokemonAtrapar[0]
                    pokemonNombre = pokemonAtrapar[2]
                    pokemonImagen = pokemonAtrapar[1]
                    nombreAsByte = str.encode(pokemonNombre)
                    lenght1 = len(nombreAsByte)
                    imagenAsByte = str.encode(pokemonImagen)
                    lenght2 = len(imagenAsByte)
                    client.sendall( struct.pack('<B',idPokemon)+struct.pack('<B', lenght1) + nombreAsByte + struct.pack('<B', lenght2) + imagenAsByte)
                elif code == 25:
                    print('Code 25')
                    codeCliente = struct.pack('<B', ConsultaPokemones)
                    numPokes = struct.pack('<B', len(clienteDB.listaPokes))
                    client.sendall(codeCliente + numPokes)
                elif code == 28:
                    codeCliente = struct.pack('<B', pokemonCap)
                    for x in clienteDB.listaPokes:
                        auxiliarimg = str(clienteDB.cadenaPokemon(x[0]))
                        nombreBits = str.encode(x[1])
                        lengt= len(nombreBits)
                        imgBits = str.encode(auxiliarimg)
                        lengt2= len(imgBits)
                        client.sendall(codeCliente + struct.pack('<B',lengt)+ nombreBits + struct.pack('<H',lengt2) + imgBits)
                        print('Enviado')
                elif code == 30:
                    print('30 recibido')
                    atrapado = clienteDB.randomAtrapar()
                    estaAtrapado = 0
                    if(atrapado):
                        estaAtrapado = 1
                        clienteDB.aniadirPokemonUsuario()
                    codeCliente = struct.pack('<B', CapturaOtra)
                    kIntentos = struct.pack('B', clienteDB.intentos)
                    respuestaAtrapado = struct.pack('B', estaAtrapado)
                    client.sendall(codeCliente +struct.pack('<B', lenght1) + nombreAsByte + kIntentos+ respuestaAtrapado)
                elif code == 31:
                    print("Abortar Atrapado")
                elif code == 32:
                    print("Conexion Cerrada")
                    #client.close()
        except:
            print("trono")
            client.close()


class Buffer:
    def __init__ (self, sock):
        self.sock = sock
        self.buffer = b''

    def get(self,length):
        while len(self.buffer) < length:
            data = self.sock.recv(7)
            if not data: break 
            self.buffer += data

        rcv = self.buffer[:length]
        self.buffer = self.buffer[length:]
        return rcv   

def bytes_a_int(bytes):
    return int.from_bytes(bytes, byteorder ="little", signed=True)
def bytes_a_string(bytes):
    return str.from_bytes(bytes, byteorder="little")

HOST = "127.0.0.1"  # Este es localhost
PORT = 9999  # Este es el puerto definido
if __name__ == "__main__":
    ThreadedServer(HOST, PORT).listen()
    print(HOST)
 
