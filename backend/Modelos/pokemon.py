class Pokemon:
    def pikachu():
        pikachuString = ""
        pikachuString+='`;-.          ___,\n'
        pikachuString+='  `.`\_...._/`.-```\n'
        pikachuString+='    \        /      ,\n'
        pikachuString+='    /()   () \    . `-._\n'
        pikachuString+='   |)  .    ()\  /   _.`\n'
        pikachuString+='   \  -`-     ,; ``. <\n'
        pikachuString+='    ;.__     ,;|   > /\n'
        pikachuString+='   / ,    / ,  |.-`.-`\n'
        pikachuString+='  (_/    (_/ ,;|.<`\n'
        pikachuString+='    \    ,     ;-`\n'
        pikachuString+='     >   \    /\n'
        pikachuString+='    (_,-`> .`\n'
        pikachuString+='        (_,`\n'
        return pikachuString
    
    def meow():
        meowString = ''
        meowString+='      .-. \_/ .-.\n'
        meowString+='      \.-\/=\/.-/\n'
        meowString+='   `-./___|=|___\.-`\n'
        meowString+='  .--| \|/`"`\|/ |--.\n'
        meowString+=' (((_)\  .---.  /(_)))\n'
        meowString+='  `\ \_`-.   .-`_/ /`_\n'
        meowString+='    `.__       __.`(_))\n'
        meowString+='        /     \     //\n'
        meowString+='       |       |__.`/\n'
        meowString+='       \       /--``\n'
        meowString+='   .--,-` .--. `----.\n'
        meowString+='  `----`--`  `--`----`\n'
        return meowString
    
    def eevee():
        evString = ''
        evString+='       ;-.               ,\n'
        evString+='        \ `.           .`/\n'
        evString+='         \  \ .---. .-` /\n'
        evString+='          `. `     `\_.`\n'
        evString+='            |(),()  |     ,\n'
        evString+='            (  __   /   .` \\\n'
        evString+='           .``.___.`--,/\_,|\n'
        evString+='          {  /     \   }   |\n'
        evString+='           `.\     /_.`    /\n'
        evString+='            |`-.-`,  `; _.`\n'
        evString+='            |  |  |   |`\n'
        evString+='            `""`""`"""` \n'
        return evString
    
    def wiggle():
        wigString = ''
        wigString+='            .~~-.      _.    \n'
        wigString+='  .``..    (_~)  ) _.-`. ;   \n'
        wigString+='  `.`..`..-(_~ _-`*. .`.`    \n'
        wigString+='    ``.`.. _ ~~  _  `;`      \n'
        wigString+='     .``. (_)   (_)  `.      \n'
        wigString+='     ;      "..."     `.     \n'
        wigString+=' .``.`.   .```-```.    `.``. \n'
        wigString+=' `.  `   ;         ;    ;  ; \n'
        wigString+='   `.   ;           ;   ` ;  \n'
        wigString+='    `.  ;           ;    ;   \n'
        wigString+='     `.  ;         ;   .`    \n'
        wigString+='     .`...:..___..:..`:.     \n'
        wigString+='  .``     ..`    `...   ~)   \n'
        return wigString
