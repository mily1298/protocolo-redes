#!/usr/bin/env python3
from Modelos.base_query import *
import mysql.connector
import random
from mysql.connector import Error
from Modelos.pokemon import *

class ClienteModelo:
    def __init__(self, correo, contrasenia):
        self.correo = correo
        self.contrasenia = contrasenia
        self.mybd = Base(correo, contrasenia)
        self.usuarioExistente = self.mybd.usuarioVerificado
        self.listaPokes = []
        self.intentos = 3
        self.pokePorAtrapar = 0
        
    def cadenaPokemon(self, idPoke):
        if(idPoke == 1):
            return Pokemon.pikachu()
        if(idPoke == 2):
            return Pokemon.meow()
        if(idPoke == 3):
            return Pokemon.eevee()
        if(idPoke == 4):
            return Pokemon.wiggle()

    def verificarUsuario(self):
        try:
            self.mybd.checarUsuario()
            self.usuarioExistente = self.mybd.usuarioVerificado
        except:
            print("trono la verificacion")
        

    def listaPokemones(self):
        try:
            self.mybd.obtenerListaPokemon()
            self.listaPokes = self.mybd.listaPokemon
        except:
            print("Error en la lista de pokemones")

    def aniadirPokemonUsuario(self):
        try:
            print(self.pokePorAtrapar)
            self.mybd.añadirPokemon(self.pokePorAtrapar)
            print('lista de Pokemones actualizadas')
            self.mybd.obtenerListaPokemon()
            self.listaPokes = self.mybd.listaPokemon
            print('lista actualizada')
        except:
            print('No se Actualizo')

    def randomPokemon(self):
        id = random.randint(1,4)
        try:
            pokemon = self.mybd.obtenerPokemon(id)[0]
            self.pokePorAtrapar = id
        except:
            pokemon = []
        return pokemon
    
    def randomAtrapar (self):
        atrapar = random.randint(1, 100)
        self.intentos -= 1
        if(atrapar > 75):
            return True
        return False
        

