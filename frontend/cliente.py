#!/usr/bin/env python3
import socket 
import struct 
import os 
import sys
import webbrowser

#Estados
PidePoke = 10
Si = 30 
No = 31
TerminaSesion = 32
IniciarSesion = 9
ConsultaPokes = 11
CapturaPoke = 20
VerPokemones = 25
ConfirmaCantidadAtrapados = 28


HOST = sys.argv[1]
PORT = int(sys.argv[2])

class Buffer:
    def __init__ (self, sock):
        self.sock = sock
        self.buffer = b''

    def get(self,length):
        while len(self.buffer) < length:
            data = self.sock.recv(1)
            if not data: break 
            self.buffer += data

        rcv = self.buffer[:length]
        self.buffer = self.buffer[length:]
        return rcv   

def bytes_a_int(bytes):
    return int.from_bytes(bytes, byteorder ="little", signed=True)
def bytes_a_string(bytes):
    return str.from_bytes(bytes, byteorder="little")


def main(): 
    if PORT == 9999 and HOST == "127.0.0.1":
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            sesion = iniciaSesion(s)
            if sesion == 8: 
                print("\n" + "Bienvenido a POKEDEX-INTERNACIONAL")
                opcion = int(eligeOpcion())
                if opcion == 1:
                    atrapaONo = int(eleccion(s,opcion))
                    menuPokeAtrapa(s, atrapaONo)
                if opcion == 2: 
                    eleccion(s, opcion) 
                if opcion == 3:
                    eleccion(s, opcion)    
            elif sesion == 7:
                print("\n" + "¡Ups! Revisa tus datos de ingreso, entrenador.")

            #s.close()
    else:
        print("POKEDEX-INTERNACIONAL no está disponible por el momento")  

def iniciaSesion(s):
    print("Inicia sesión...")
    correo = input("Ingresa tu correo: ")
    contrasenia = input("Ingresa tu contraseña: ")
    correoAsBytes = str.encode(correo)
    contraseniaAsBytes = str.encode(contrasenia)
    subBuffer1 = len(correoAsBytes) 
    subBuffer2 = len(contraseniaAsBytes)
    code = struct.pack('<B', IniciarSesion)
    s.sendall(code + struct.pack('<B', subBuffer1) + correoAsBytes + struct.pack('<B', subBuffer2) + contraseniaAsBytes)
    respuestaSesion = struct.unpack('<B', s.recv(1))[0]
    return respuestaSesion

def eligeOpcion():
    print("\n" + "¡Muy bien, entrenador! Dinos, hoy es un lindo día para...")
    print("\n" + "1: ¡Atrapar un pokemon!")
    print("2: Saber qué pokemones tengo")
    print("3: Salir de POKEDEX-INTERNACIONAL")
    eleccion = input("\n" + "Escribe el número de tu elección: " )
    return eleccion

def eleccion(s, eleccion):
    if eleccion == 1:
        code = struct.pack('<B', PidePoke)
        s.sendall(code)
        buf = Buffer(s)
        idPokemon = struct.unpack('<B', buf.get(1))[0]
        nombreBuf = struct.unpack('<B', buf.get(1))[0]
        pokeNombre = buf.get(nombreBuf)
        nombre = pokeNombre.decode()
        urlBuf = struct.unpack('<B', buf.get(1))[0]
        pokeUrl = buf.get(urlBuf)
        url = pokeUrl.decode()
        print("\n" + "Te ofrecemos el pokemon... " + "¡"+nombre+"!")
        print("¿Te interesa tenerlo?")
        print("\n" + "1: Yay! Capturar AHORA!")
        print("2: Emmm... Nope")
        opcion = input("\n" + "Escribe el número de tu elección: " )
        return opcion
    elif eleccion == 2:
        print("A continuación, tus pokemones: " + "\n")
        code = struct.pack('<B', VerPokemones)
        s.sendall(code)
        buf = Buffer(s)
        code = struct.unpack('<B', buf.get(1))[0]
        numPokemones = struct.unpack('<B', buf.get(1))[0]
        code1 = struct.pack('<B', ConfirmaCantidadAtrapados)
        s.sendall(code1)
        for i in range (0,numPokemones):
            buf1 = Buffer(s)
            code1 = struct.unpack('<B', buf1.get(1))[0]
            tamañoNombre = struct.unpack('<B', buf1.get(1))[0]
            nombre1AsByte = buf1.get(tamañoNombre)
            nombre1 = nombre1AsByte.decode()
            print(nombre1)
            tamañoImg = struct.unpack('<H', buf1.get(2))[0]
            bufferImageAsByte = buf1.get(tamañoImg)
            bufferImage = bufferImageAsByte.decode()
            print(bufferImage)

    elif eleccion == 3:
        code = struct.pack('<B', TerminaSesion)
        s.sendall(code)
        s.close()
        print("\n" + "¡Vuelve pronto, entrenador!")

def menuPokeAtrapa(s, opcion):
    if opcion == 2: 
        code = struct.pack('<B', No)
        s.sendall(code)
        print("¡Vuelve pronto, entrenador!")
        s.close()
    elif opcion == 1:
        while True:
            code = struct.pack('<B', Si)
            s.sendall(code)
            buf = Buffer(s)
            code = struct.unpack('<B', buf.get(1))[0]
            lenghtNombre = struct.unpack('<B', buf.get(1))[0]
            nombreByte = buf.get(lenghtNombre)
            nombreAtrapado = nombreByte.decode()
            intentos = struct.unpack('<B', buf.get(1))[0]
            statusAtrapado = struct.unpack('<B', buf.get(1))[0]
            if statusAtrapado == 0: 
                print("\n" + "¡No atrapaste a " + nombreAtrapado + "!" + "\n" + "Te quedan " + str(intentos) + " intentos.")
                if (intentos > 0):
                    print("\n" + "¿Quieres intentarlo nuevamente?")
                    print("1: ¡Sí! " + nombreAtrapado+" debe ser mío.")
                    print("2: Mñeeeh, nop.")
                    eleccion = input("\n" + "Escribe el número de tu elección: " )
                    if(eleccion == "2"):
                        print("Entendemos que le hayas temido al éxito, entrenador. ¡Vuelve pronto!")
                        return False                   
            else:
                print("Has atrapado a " + nombreAtrapado +"."+ " ¡Felicidades, entrenador!")
                return False
            if(int(intentos)== 0):
                return False
            



if __name__ == "__main__":
    main()
